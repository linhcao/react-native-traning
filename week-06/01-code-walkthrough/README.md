# Week 6

### Roadmap

 - Animation
 - WebView 
 - Socket IO

#### 1. Animation

 - 3 cách tạo Animation object:

 * Animated.timing()
 * Animated.decay()
 * Animated.spring()

 - 3 cách gọi animation thực thi:

 * Animated.parallel()
 * Animated.sequence()
 * Animated.stagger()

##### `Animated.timing()`

<img src="./image/animation_timing.gif" width="300px"/>

 - Import module:

<img src="./image/img-import.png" width="300px"/>

 - Setup animation:

<img src="./image/img-setup.png" width="600px"/>

 - Create value for animation:

<img src="./image/img-create-value.png" width="600px"/>

 - Render animation:

<img src="./image/img-render.png" width="600px"/>

 - `renderAnimationView()`

<img src="./image/img-render-function.png" width="600px"/>

##### `Animated.spring()`

<img src="./image/animation_spring.gif" width="300px"/>

 - Setup:

<img src="./image/spr-setup.png" width="600px"/>

 - Render animation:

<img src="./image/spr-render.png" width="600px"/>

 - Functions:

<img src="./image/spr-function.png" width="600px"/>

#### 2. WebView

 - Load link:

<img src="./image/web-link.png" width="600px"/>

 - Load html string:

<img src="./image/web-html.png" width="600px"/>

 - Event:

 * `onError`
 * `onLoad`
 * `onLoadEnd`
 * `onLoadStart`

 [SEE DOCS](https://facebook.github.io/react-native/docs/webview)

 - Communicating between React Native and the WebView:

 <img src="./image/webview.gif" width="300px"/>

 - `postMessage` & `onMessage`:

 <img src="./image/web-event.png" width="600px"/>

 - `index.html`:

 <img src="./image/web-index.png" width="600px"/>

