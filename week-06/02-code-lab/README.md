## Lab week 6

### Roadmap

 - React Context
 	- Create Context
 	- Provider
 	- Consumer
 	- Action

 - Locales with react-native-i18n

### React Context

- Create context

<img src="./image/app-context.png" width="600px"/>

- Create App provider

<img src="./image/app-provider.png" width="600px"/>

- Use provider in `App.js`

<img src="./image/app-use.png" width="600px"/>

- Consumer in `HomeScreen`

<img src="./image/app-home.png" width="600px"/>
