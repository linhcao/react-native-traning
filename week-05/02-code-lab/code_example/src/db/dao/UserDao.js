import BaseDao from "./BaseDao";

export default class UserDao extends BaseDao {
    constructor() {
        super('User');
    }
}