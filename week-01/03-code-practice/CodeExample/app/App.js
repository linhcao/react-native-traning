import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {FONT_MEDIUM} from "./fonts/fonts";
import Button from "./Button";

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orientation: false,
            count: 0
        }
    }

    render() {

        return (
            <View
                style={[styles.container]}
                onLayout={this.onViewLayout}
            >
                <Button
                    style={{
                        height: 100
                    }}
                    textStyle={{
                        fontSize: 20,
                        color: '#ff0000'
                    }}
                    count={this.state.count.toString()}
                >Count</Button>
                <Button
                    onButtonClick={this.onButtonClick}
                    textStyle={{
                        fontSize: 30
                    }}
                >+</Button>
                <Button
                    onButtonClick={this.onButtonClick}
                    textStyle={{
                        fontSize: 30
                    }}
                >-</Button>
            </View>
        );
    }

    onButtonClick = (text) => {
        switch (text) {
            case '+':
                this.setState({count: this.state.count + 1});
                break;
            case '-':
                this.setState({count: this.state.count - 1});
                break;
            default:

        }
    };

    renderVertical = () => {
        return (
            <Text style={{
                fontSize: 35,
                fontFamily: FONT_MEDIUM
            }}>Vertical</Text>
        );
    };

    renderHorizontal = () => {
        return (
            <Text style={{
                fontSize: 35,
                fontFamily: 'AvenirNext-Medium'
            }}>Horizontal</Text>
        );
    };

    onViewLayout = (event) => {
        const layout = event && event.nativeEvent && event.nativeEvent.layout;
        if (layout) {
            let orientation = layout.width > layout.height;
            this.setState({orientation});
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});
