import React, {Component} from 'react';
import {AppContext} from './AppContext'

export default class AppProvider extends Component {

    state = {
        value: 0,
        change: () => {
            this.setState({value: this.state.value + 1});
        }
    };

    render() {
        return (
            <AppContext.Provider
                value={this.state}
            >
                {this.props.children}
            </AppContext.Provider>
        );
    }
}







